import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {HttpClientModule} from '@angular/common/http';
import { DatePipe } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AgGridModule } from 'ag-grid-angular';
import 'ag-grid-enterprise';
import { GridComponentComponent } from './components/grid-component/grid-component.component';
import { ImgRendererComponent } from './components/grid-component/img-renderer/img-renderer.component';
import { ToolPanelComponent } from './components/grid-component/tool-panel/tool-panel.component';
@NgModule({
  declarations: [
    AppComponent,
    GridComponentComponent,
    ImgRendererComponent,
    ToolPanelComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    AgGridModule.withComponents([ImgRendererComponent, ToolPanelComponent])
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
