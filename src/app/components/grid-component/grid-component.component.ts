import { Component, OnInit, OnDestroy } from '@angular/core';

import { Subscription } from 'rxjs';

import { ImgRendererComponent } from './img-renderer/img-renderer.component';
import { ToolPanelComponent } from './tool-panel/tool-panel.component';

import { DataService } from 'src/app/services/data.service';
import { DatePipe } from '@angular/common';

import { Column, RowNode, GridApi, CellClickedEvent, SelectionChangedEvent, GridOptions } from 'ag-grid-community';
import { GoogleDataItem } from '../../interfaces';
interface GetContextMenuItemsParams {
  column: Column; // the column that was clicked
  node: RowNode; // the node that was clicked
  value: any; // the value displayed in the clicked cell
  api: GridApi; // the grid API
  columnApi: any; // the column API
  // columnApi: ColumnAPI; // the column API
  context: any; // the grid context
  defaultItems: string[]; // names of the items that would be provided by default

}

@Component({
  selector: 'app-grid-component',
  templateUrl: './grid-component.component.html',
  styleUrls: ['./grid-component.component.scss']
})
export class GridComponentComponent implements OnInit, OnDestroy {
  subsriptions: Subscription[] = [];
  gridData: GoogleDataItem[];
  gridOptions: GridOptions = {
    getRowHeight: (params) => {
      return params.data.rowHeight;
    }, defaultColDef: {
      sortable: true,
      resizable: true,
      filter: true
    },
    rowSelection: 'multiple',
    frameworkComponents: {
      ToolPanelComponent
    },

    sideBar: {
      toolPanels: [
        {
          id: 'customStats',
          labelDefault: 'Custom Stats',
          labelKey: 'customStats',
          iconKey: 'custom-stats',
          toolPanel: 'ToolPanelComponent'
        }
      ],
      defaultToolPanel: 'customStats'
    }

    // columnHeaders:() => this.columnHeaders
  };
  constructor(
    private dataService: DataService,
    private datePipe: DatePipe) { }
  columnHeaders: any[] = [
    {
      headerName: '',
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      headerName: '', field: 'imgUrl', cellRendererFramework: ImgRendererComponent
    },
    {
      headerName: 'Published on',
      field: 'publishedAt',
      valueFormatter: (date) => {
        return this.datePipe.transform(date.value, 'yyyy-MM-dd');
      }
    },
    { headerName: 'Video Title', field: 'title' },
    { headerName: 'Description', field: 'description' }
  ];

  cellClickHandler(e: CellClickedEvent) {
    if (e.colDef.field === 'title') {
      this.navigateToYoutube(e.data.youtubeUrl);
    }
  }
  navigateToYoutube(url: string) {
    window.open(url, '_blank').focus();
  }
  getContextMenuItems(params: GetContextMenuItemsParams) {
    if (params.column.getColDef().field === 'title') {
      return ['copy', 'export', 'autoSizeAll', {
        name: 'open in new tab',
        action: () => {
          window.open(params.node.data.youtubeUrl, '_blank').focus();
        },
      }];
    }
    return [];
  }
  onGridReady = params => {
    // Following line to make the currently visible columns fit the screen
    params.api.sizeColumnsToFit();

    // Following line dymanic set height to row on content
    params.api.resetRowHeights();
  }
  ngOnInit() {
    this.subsriptions.push(
      this.dataService.googleData.subscribe(res => {
        this.gridData = res.map(i => {
          return {
            // here height should take from thumbnails
            ...i, autoHeight: true, width: 100, rowHeight: 90
          };
        });
      })
    );
  }
  ngOnDestroy() {
    this.subsriptions.forEach(i => {
      i.unsubscribe();
    });
  }

}
