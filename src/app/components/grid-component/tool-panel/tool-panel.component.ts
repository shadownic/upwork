import { Component, OnInit, OnChanges } from '@angular/core';
import { IToolPanel, IToolPanelParams } from 'ag-grid-community/dist/lib/interfaces/iToolPanel';

@Component({
  selector: 'app-tool-panel',
  templateUrl: './tool-panel.component.html',
  styleUrls: ['./tool-panel.component.scss']
})
export class ToolPanelComponent implements IToolPanel {
  total: number;
  selected: number;
  private params: IToolPanelParams;
  eventListener;
  constructor() { }

  agInit(params: IToolPanelParams) {
    this.params = params;
    this.selected = 0;
    this.total = 0;
    this.params.api.addEventListener('modelUpdated', this.refresh.bind(this));
    this.params.api.addEventListener('selectionChanged', this.refresh.bind(this));

  }
  revert() {


    this.params.api.forEachNode((node) => {

      if (node.isSelected()) {
        node.selectThisNode(false);
      } else {
        node.selectThisNode(true);
      }


      this.refresh();
    });
  }
  refresh() {
    let total = 0;
    this.params.api.forEachNode((node) => {
      total++;
    });
    // this.params.api
    this.total = total;
    this.selected = this.params.api.getSelectedRows().length || 0;
  }
}
