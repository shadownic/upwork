export interface DataApiResponse {
    etag: string;
    items: GooglaDataItemResponse[];
    pageInfo: {
        resultsPerPage: number
        totalResults: number
    };
}
export interface GooglaDataItemResponse {
    etag: string;
    kind: string;
    id: {
        kind: string;
        videoId: string;
    };
    snippet: {
        channelId: string;
        channelTitle: string;
        description: string;
        liveBroadcastContent: string;
        publishedAt: Date;
        thumbnails: {
            default: {
                height: number;
                url: string;
                width: number;
            }
            high: {
                height: number;
                url: string;
                width: number;
            }
            medium:
            {
                height: number;
                url: string;
                width: number;
            }
        }
        title: string
    };

}
export interface GoogleDataItem {
    imgUrl: string;
    publishedAt: Date;
    title: string;
    description: string;
    youtubeUrl: string;
// // publishedAt as Published on
// title as Video Title
// description as Description
}
