import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
// this is static url so we dont use as domain for requeests
// tslint:disable-next-line:max-line-length
const apiUrl = 'https://www.googleapis.com/youtube/v3/search?key=AIzaSyDOfT_BO81aEZScosfTYMruJobmpjqNeEk&maxResults=50&type=video&part=snippet&q=john';
interface Options {
  headers?: HttpHeaders | {
    [header: string]: string | string[];
  };
  observe?: 'body';
  params?: HttpParams | {
    [param: string]: string | string[];
  };
  reportProgress?: boolean;
  responseType?: 'json';
  withCredentials?: boolean;

}
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public admin: boolean;

  constructor(private http: HttpClient) { }
  get(url: string, options?: Options) {
    return this.http.get(apiUrl + url, options);
  }
  post(url: string, body: any, options?: Options) {
    return this.http.post(apiUrl + url, body, options);
  }
  put(url: string, body: any, options?: Options) {
    return this.http.put(apiUrl + url, body, options);
  }
}
