import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { ApiService } from './api.service';

import { DataApiResponse, GooglaDataItemResponse, GoogleDataItem } from '../interfaces';

const youtubeUrl = 'https://www.youtube.com/watch?v=';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  googleData = new ReplaySubject<GoogleDataItem[]>(1);

  constructor(private apiService: ApiService) {
    // as it root service we provide first data as first call
    this.updateData();
  }

  updateData() {
    this.apiService.get('').toPromise().then((res: DataApiResponse) => {
      // cut data we dont need
      const result: GoogleDataItem[] = res.items.map(item => {
        const snippet = item.snippet;
        const usedItem: GoogleDataItem = {
          title: snippet.title,
          description: snippet.description,
          // for Grid we use small image
          imgUrl: snippet.thumbnails.default.url,
          youtubeUrl: `${youtubeUrl}${snippet.channelId}`,
          publishedAt: snippet.publishedAt
        };
        return usedItem;
      });
      this.googleData.next(result);
    }).catch(e => {
      console.error(e);
    });
  }
}
